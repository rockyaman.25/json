package com.example.demo;

import com.example.demo.Demo.PricingPolicyDetails;
import com.example.demo.Demo.ServiceCharge;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

@SpringBootApplication
public class JsonApplication {

	public static void main(String[] args) {

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			PricingPolicyDetails pricingPolicyDetails = dummyValues();

			// Parsing the HashMap from the main JSON File
			HashMap hashMap = new ObjectMapper().readValue(new File("src/main/resources/context2.json"), HashMap.class);
			System.out.println("Complete JSON File: " + hashMap);

			// Nested HashMap
			Map map2 = (HashMap) ((Map)hashMap.get("originalTransactionDetails")).get("pricingPolicyDetails");
			System.out.println("Printing pricingPolicyDetails: " + map2);

			// Converting ServiceCharge Map --->> List
			List<Map> list = (List<Map>) map2.get("serviceCharges");
			System.out.println("Services Charges List: " + list);

			// Mapping value from Java Class object
			String node = objectMapper.writeValueAsString(pricingPolicyDetails);
			System.out.println("ServiceCharges Updated: "+node);

		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private static PricingPolicyDetails dummyValues(){
		// HashMap for Receiver
		Map map0 = new HashMap();
		map0.put("productId","12");
		map0.put("isValid", true);
		map0.put("gradeCode","OPT");
		map0.put("partyId","INC9a0810101");
		map0.put("accountNumber", "INC9a0810101");
		map0.put("isAccountValid", true);

		// HashMap for Sender
		Map map1 = new HashMap();
		map1.put("productId","12");
		map1.put("isValid", true);
		map1.put("accountNumber","101120000000168");
		map1.put("partyId","US.2121642766246583");
		map1.put("isAccountValid",true);

		// List for taxes
		List<Integer> list = new ArrayList();

		// HashMap for ServiceCharges
		Map map2 = new HashMap();
		map2.put("amount","00");
		map2.put("isInclusive",true);
		map2.put("taxes",list);
		map2.put("receiver",map0);
		map2.put("sender",map1);

		PricingPolicyDetails pricingPolicyDetails = new PricingPolicyDetails();

		List<Map> serviceCharges = new ArrayList<>();
		serviceCharges.add(map2);

		// Setting value of ServiceCharges
		pricingPolicyDetails.setServiceCharges(serviceCharges);
		System.out.println("print: "+serviceCharges);
		return pricingPolicyDetails;

	}

}


package com.example.demo.Demo;
import java.util.List;


public class ServiceCharge {
    public Integer amount;
    public Receiver receiver;
    public Sender sender;
    public Boolean isInclusive;
    public List<Object> taxes = null;
    public String chargeName;


    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Boolean getInclusive() {
        return isInclusive;
    }

    public void setInclusive(Boolean inclusive) {
        isInclusive = inclusive;
    }

    public List<Object> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Object> taxes) {
        this.taxes = taxes;
    }

    public String getChargeName() {
        return chargeName;
    }

    public void setChargeName(String chargeName) {
        this.chargeName = chargeName;
    }

    @Override
    public String toString() {
        return "ServiceCharge{" +
                "amount=" + amount +
                ", receiver=" + receiver +
                ", sender=" + sender +
                ", isInclusive=" + isInclusive +
                ", taxes=" + taxes +
                ", chargeName='" + chargeName + '\'' +
                '}';
    }
}
package com.example.demo.Demo;

public class Receiver {

    public String productId;
    public Boolean isValid;
    public String gradeCode;
    public String partyId;
    public String accountNumber;
    public Boolean isAccountValid;

    @Override
    public String toString() {
        return "Receiver{" +
                "productId='" + productId + '\'' +
                ", isValid=" + isValid +
                ", gradeCode='" + gradeCode + '\'' +
                ", partyId='" + partyId + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", isAccountValid=" + isAccountValid +
                '}';
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }

    public String getGradeCode() {
        return gradeCode;
    }

    public void setGradeCode(String gradeCode) {
        this.gradeCode = gradeCode;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Boolean getAccountValid() {
        return isAccountValid;
    }

    public void setAccountValid(Boolean accountValid) {
        isAccountValid = accountValid;
    }
}
package com.example.demo.Demo;

import java.util.List;
import java.util.Map;

public class PricingPolicyDetails {

    public List<Map> serviceCharges;

    @Override
    public String toString() {
        return "PricingPolicyDetails{" +
                "serviceCharges=" + serviceCharges +
                '}';
    }

    public List<Map> getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(List<Map> serviceCharges) {
        this.serviceCharges = serviceCharges;
    }
}
package com.example.demo.Demo;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


public class Sender {

    public String productId;
    public Boolean isValid;
    public String accountNumber;
    public String partyId;
    public Boolean isAccountValid;

    @Override
    public String toString() {
        return "Sender{" +
                "productId='" + productId + '\'' +
                ", isValid=" + isValid +
                ", accountNumber='" + accountNumber + '\'' +
                ", partyId='" + partyId + '\'' +
                ", isAccountValid=" + isAccountValid +
                '}';
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public Boolean getAccountValid() {
        return isAccountValid;
    }

    public void setAccountValid(Boolean accountValid) {
        isAccountValid = accountValid;
    }
}